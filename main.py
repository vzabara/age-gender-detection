import os
import shutil
from datetime import datetime
from pathlib import Path
from PIL import Image
from fastapi import FastAPI, Header, File, UploadFile
import configparser
import urllib3
from numpy import asarray
import cv2


urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


MODEL_MEAN_VALUES = (78.4263377603, 87.7689143744, 114.895847746)
age_list = ['0..2', '4..6', '8..12', '15..20', '25..32', '38..43', '48..53', '60..100']
gender_list = ['male', 'female']


def initialize_caffe_model():
    age = cv2.dnn.readNetFromCaffe(
        "models/deploy_age.prototxt",
        "models/age_net.caffemodel")
    gender = cv2.dnn.readNetFromCaffe(
        "models/deploy_gender.prototxt",
        "models/gender_net.caffemodel")

    return (age, gender)


app = FastAPI()
working_dir = os.path.dirname(os.path.abspath(__file__))
config = configparser.ConfigParser()
config.read(working_dir + '/config.ini')
upload_dir = config['API']['uploads']

if not Path(upload_dir).is_dir():
    Path(upload_dir).mkdir(parents=True, exist_ok=True)

classifier = config['API']['haarcascades'] + 'haarcascade_frontalface_alt.xml'
age_net, gender_net = initialize_caffe_model()


@app.post('/api/v1/detect')
async def gender_v1(authorization: str = Header(None), file: UploadFile = File(...)):
    return detect_gender(authorization, file)


def check_token(authorization):
    if authorization == '':
        return False
    parts = authorization.split(' ')
    if parts[1] == '' or parts[1] != config['API']['token']:
        return False
    return True


def detect_gender(authorization: str = Header(None), image: UploadFile = File(...)):
    if not check_token(authorization):
        return {
            'success': False,
            'message': 'Unauthorized'
        }
    parts = image.content_type.split("/")
    if parts[0] != "image" or (parts[1] != "png" and parts[1] != "jpeg"):
        return {
            'success': False,
            'message': 'Unsupported file format'
        }
    else:
        file_name = "%s/%s-%s" % (upload_dir, datetime.now(), image.filename)
        with open(file_name, "wb") as buffer:
            shutil.copyfileobj(image.file, buffer)
        img = asarray(Image.open(file_name))
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        face_cascade = cv2.CascadeClassifier(classifier)
        faces = face_cascade.detectMultiScale(gray, 1.1, 5)
        results = []
        for (x, y, w, h) in faces:
            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 255, 0), 2)
            face_img = img[y:y + h, x:x + w].copy()
            blob = cv2.dnn.blobFromImage(face_img, 1, (227, 227), MODEL_MEAN_VALUES, swapRB=False)
            gender_net.setInput(blob)
            gender_preds = gender_net.forward()
            gender = gender_list[gender_preds[0].argmax()]
            age_net.setInput(blob)
            age_preds = age_net.forward()
            age = age_list[age_preds[0].argmax()]
            results.append({
                "gender": gender,
                "age": age,
                "area": {
                    "x": int(x),
                    "y": int(y),
                    "w": int(w),
                    "h": int(h)
                }
            })
        os.system('rm "%s"' % file_name)
        if len(faces) == 0:
            return {
                "success": False,
                'message': 'No faces have been detected'
            }
        else:
            return {
                "success": True,
                "detected": results
            }
