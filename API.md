# Age / Gender Detector API

**Version 1.0**

### Detect method

Request

```sh
$ curl https://gender-detector.xray.instamaven.com/api/v1/detect \
  -X POST \
  -H "Authorization: Bearer LY4aMRHZOTnXrufrEm8Dq8AAebFAgNUaq3k9MgsYetJxwcoR7C2USP6bkfZk" \
  -F "file=@family.jpg"
```

Response

```json
{
    "success": true,
    "detected": [
        {
            "gender": "male",
            "age": "25..32",
            "area": {
                "x": 330,
                "y": 88,
                "w": 141,
                "h": 141
            }
        },
        {
            "gender": "female",
            "age": "15..20",
            "area": {
                "x": 28,
                "y": 76,
                "w": 141,
                "h": 141
            }
        }
    ]
}
```

## Installation

>
> Change **xray** to your actual user name
>

 - Copy files to **/usr/local/bin/gender-detector** directory
 - Set parameters in **config.ini** file
 - install Python packages:

```sh
sudo apt install python3-opencv
sudo su - xray
pip3 install fastapi configparser uvicorn[standard] python-multipart pillow numpy
```

- copy files from **https://github.com/opencv/opencv/tree/master/data/haarcascades** to **/usr/share/opencv4/haarcascades**

- create **/etc/systemd/system/gender-detector.service** file:

```ini
[Unit]
Description=Gender Detector Systemd Service
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
ExecStart=/home/xray/.local/bin/uvicorn main:app --port 5001
User=xray
Group=xray
RuntimeDirectory=/usr/local/bin/gender-detector
WorkingDirectory=/usr/local/bin/gender-detector
ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
TimeoutStopSec=5
PrivateTmp=true
RestartSec=10
Restart=always

[Install]
WantedBy=multi-user.target
```
 - start Uvicorn servce:
```sh
sudo systemctl start gender-detector.service
sudo systemctl enable gender-detector.service
```
