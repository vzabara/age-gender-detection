# Age / Gender detection
### File ./photos/female15.png, Found 0 face(s)

| Age | Gender |
|-----|--------|
| n/a | n/a |

![./photos/female15.png](./photos/female15.png)

### File ./photos/female12.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (48, 53) | Female |

![./photos/female12.png](./photos/female12.png) : ![./detected/female12.png](./detected/female12.png)

### File ./photos/female8.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (15, 20) | Female |

![./photos/female8.png](./photos/female8.png) : ![./detected/female8.png](./detected/female8.png)

### File ./photos/female14.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (8, 12) | Male |

![./photos/female14.png](./photos/female14.png) : ![./detected/female14.png](./detected/female14.png)

### File ./photos/female9.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (48, 53) | Male |

![./photos/female9.png](./photos/female9.png) : ![./detected/female9.png](./detected/female9.png)

### File ./photos/female7.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (15, 20) | Female |

![./photos/female7.png](./photos/female7.png) : ![./detected/female7.png](./detected/female7.png)

### File ./photos/female5.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (38, 43) | Female |

![./photos/female5.png](./photos/female5.png) : ![./detected/female5.png](./detected/female5.png)

### File ./photos/female13.png, Found 0 face(s)

| Age | Gender |
|-----|--------|
| n/a | n/a |

![./photos/female13.png](./photos/female13.png)

### File ./photos/male7.png, Found 2 face(s)

| Age | Gender |
|-----|--------|
| (25, 32) | Male |
| (15, 20) | Male |

![./photos/male7.png](./photos/male7.png) : ![./detected/male7.png](./detected/male7.png)

### File ./photos/female6.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (15, 20) | Male |

![./photos/female6.png](./photos/female6.png) : ![./detected/female6.png](./detected/female6.png)

### File ./photos/male6.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (25, 32) | Male |

![./photos/male6.png](./photos/male6.png) : ![./detected/male6.png](./detected/male6.png)

### File ./photos/female2.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (38, 43) | Female |

![./photos/female2.png](./photos/female2.png) : ![./detected/female2.png](./detected/female2.png)

### File ./photos/male1.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (60, 100) | Male |

![./photos/male1.png](./photos/male1.png) : ![./detected/male1.png](./detected/male1.png)

### File ./photos/male4.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (25, 32) | Male |

![./photos/male4.png](./photos/male4.png) : ![./detected/male4.png](./detected/male4.png)

### File ./photos/male3.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (38, 43) | Male |

![./photos/male3.png](./photos/male3.png) : ![./detected/male3.png](./detected/male3.png)

### File ./photos/female10.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (25, 32) | Male |

![./photos/female10.png](./photos/female10.png) : ![./detected/female10.png](./detected/female10.png)

### File ./photos/female3.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (38, 43) | Female |

![./photos/female3.png](./photos/female3.png) : ![./detected/female3.png](./detected/female3.png)

### File ./photos/female1.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (0, 2) | Female |

![./photos/female1.png](./photos/female1.png) : ![./detected/female1.png](./detected/female1.png)

### File ./photos/male2.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (48, 53) | Male |

![./photos/male2.png](./photos/male2.png) : ![./detected/male2.png](./detected/male2.png)

### File ./photos/female4.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (48, 53) | Female |

![./photos/female4.png](./photos/female4.png) : ![./detected/female4.png](./detected/female4.png)

### File ./photos/male5.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (38, 43) | Male |

![./photos/male5.png](./photos/male5.png) : ![./detected/male5.png](./detected/male5.png)

### File ./photos/female11.png, Found 2 face(s)

| Age | Gender |
|-----|--------|
| (0, 2) | Male |
| (25, 32) | Male |

![./photos/female11.png](./photos/female11.png) : ![./detected/female11.png](./detected/female11.png)

### File ./photos/female16.png, Found 1 face(s)

| Age | Gender |
|-----|--------|
| (15, 20) | Male |

![./photos/female16.png](./photos/female16.png) : ![./detected/female16.png](./detected/female16.png)

