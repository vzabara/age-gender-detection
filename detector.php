<?php

function rrmdir($src) {
    $dir = opendir($src);
    while(false !== ( $file = readdir($dir)) ) {
        if (( $file != '.' ) && ( $file != '..' )) {
            $full = $src . '/' . $file;
            if ( is_dir($full) ) {
                rrmdir($full);
            }
            else {
                unlink($full);
            }
        }
    }
    closedir($dir);
    rmdir($src);
}


if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES['file'])) {
    $original_name = $_FILES['file']['name'];
    if (!empty($original_name)) {
        // try to detect gender and save results
        $date = date('c');
        $uploaddir = __DIR__ . "/results-$date/";
        if (!file_exists($uploaddir)) {
            mkdir($uploaddir);
        }
        $uploadfile = $uploaddir . 'original';
        if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
            $mime = mime_content_type($uploadfile);
            $parts = explode('/', $mime);
            $file_name = $uploadfile . ".$parts[1]";
            rename($uploadfile, $file_name);
            file_put_contents(sprintf("%s/file.txt", $uploaddir), $original_name);
            ob_start();
            exec(sprintf("/usr/bin/python3 %s/__detect.py --photo=%s", __DIR__, $file_name), $res);
            ob_end_flush();
            $messages = "";
            array_shift($res);
            foreach ($res as $message) {
                $messages .= "<li>$message</li>";
            }
            $seek = sprintf("%s", str_replace("original", "detected", $file_name));
            $file_name = str_replace(__DIR__, "", $file_name);
            if (file_exists($seek)) {
                $seek = str_replace(__DIR__, "", $seek);
                $html = <<<EOL1
<html>
<head><title>Detection Result</title></head>
<body>
<ul>
    $messages
</ul>
<table>
<tr>
    <td><img src="$file_name" /></td>
    <td><img src="$seek" /></td>
</tr>
</table>
</body>
</html>
EOL1;
            } else {
                $html = <<<EOL2
<html>
<head><title>Detection Result</title></head>
<body>
<ul>
    $messages
</ul>
<table>
<tr>
    <td><img src="$file_name" /></td>
</tr>
</table>
</body>
</html>
EOL2;
            }
            file_put_contents(sprintf("%s/index.html", $uploaddir), $html);
        }
    }
}

if (!empty($_REQUEST['remove'])) {
    rrmdir($_REQUEST['remove']);
}
?>
<html>
<head>
    <title>Detector Demo Page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
            crossorigin="anonymous"></script>
    <script
            src="https://code.jquery.com/jquery-2.2.4.min.js"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
            crossorigin="anonymous"></script>
</head>
<body style="padding: 1rem">
<section>
    <h3>Upload photo</h3>
    <p>Max file size is 10MB</p>
    <form method="POST" enctype="multipart/form-data" action="detector.php">
        <label>File <input type="file" name="file"></label>
        <button type="submit">Send</button>
    </form>
</section>
<p>&nbsp;</p>
<section>
    <h3>Previous results</h3>
    <ul>
        <?php
        $dirs = [];
        $dir = new DirectoryIterator('.');
        foreach ($dir as $fileinfo) {
            if ($fileinfo->isDir() && !$fileinfo->isDot()) {
                $entry = $fileinfo->getFilename();
                $files = glob("$entry/detected.*");
                $success = count($files) > 0 ? "<span class='text-success'>Success</span>" : "<span class='text-danger'>Failed</span>";
                $original = file_get_contents("$entry/file.txt");
                if (!empty($original) && substr($entry, 0, 7) == 'results') {
                    $dirs[$fileinfo->getMTime()] = "<li><a href='/$entry/index.html'>$original</a> ($success) - <a href='javascript:remove(\"$entry\")'>remove</a></li>";
                }
            }
        }
        krsort($dirs);
        foreach ($dirs as $dir){
            echo $dir;
        }
        ?>
    </ul>
</section>
<script>
  function remove(directory) {
    if (confirm('Are you sure?')) {
        $.ajax({
            'url': '/detector.php',
            'data': {
                'remove': directory
            }
        }).done(function() {
            document.location.href = "/detector.php";
        });
    }
    return false;
}
</script>
</body>
</html>
